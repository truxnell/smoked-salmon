# smoked-salmon

A tool to assist with finding music and uploading it to RED.

All information pertaining to its use can be found in the wiki.

Wiki: https://codeberg.org/xmoforf/smoked-salmon/wiki/home

## Credits

This is a fork of the wonderful tool provided by **ligh7s** at 
https://github.com/ligh7s/smoked-salmon. Massive credit and thanks to them
for sharing this tool.

The git log also indicates the following contributors:

```
$ git shortlog -s -n
   100  elwood
     9  elwoodpd
     7  bootldrDNB
     7  xmoforf
     6  flying-sausages
     4  lights
     3  b o o t l d r
     2  AltQ
     2  Flying Sausages
     1  Henry Romp
     1  Junkbite
     1  bootldr
     1  flying sausages
```

### Plugin Installationq

Clone plugins into the plugins/ folder. Salmon will automatically detect
and import them. Their CLI commands should appear when salmon is next ran.

### Colors

The different terminal colors used throughout salmon will generally stick to the
following pattern of use.

- **Default** - Information on what salmon is doing
- **Red** - Failure, urgent, requires attention
- **Green** - Success, no problems found
- **Yellow** - Information block headers
- **Cyan** - Section headers
- **Magenta** - User prompts, attention please!

### Testimonials

```
Salmon filled the void in my heart. I no longer chase after girls. ~boot
With the help of salmon, I overcame my addition to kpop thots. ~b
I warn 5 people every day on the forums using salmon! ~jon
```

---

The Salmon Icon made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by
<a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0"
target="_blank">CC 3.0 BY</a>.
